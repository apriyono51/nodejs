const Koa = require('koa');
const routes = require('koa-router');
const app = new Koa();
const koaBody = require('koa-body')
const route = new routes();
const elektronik = require('./elektronik');

let array = [
{
	"id" : 1,
	"name" : "sharp32",
	"model" : "tabung",
	"type" : "televisi"
},
{
	"id" : 2,
	"name" : "LG86",
	"model" : "2_pintu",
	"type" : "kulkas"
}
];

route.get('/list',(ctx)=>{
	let data = array
    if (ctx.request.query.name) {
        data = array.filter(data => {
            return data.name === ctx.request.query.name
        })
    }

	ctx.body = {
		status: 200,
		message : "ok",
		data : data
	};
});

route.get('/list/:id',(ctx)=>{
	let data = array.find(item => {
        return item.id === Number(ctx.params.id)
    })

    ctx.body = {
        status: 200,
        message: 'OK',
        results: data
    }
});

route.post('/create',koaBody(),(ctx)=>{
	let data = elektronik(ctx.request.body);
        array.push(data);

            return ctx.body = {
                status: 200,
                message: 'OK',
                results: array
            };
})

route.put('/update/:id',koaBody(),(ctx)=>{
	const paramsId = ctx.params.id;

	for(const i in array) {
		if (array[i].id === Number(paramsId)) {
			array[i].name = ctx.request.body.name;
			array[i].model = ctx.request.body.model;

			break;
		}
	}

	ctx.body = {
		status: 200,
		message : "Ok",
		result : "Update Success"
	};
})

route.delete('/delete',(ctx)=>{
	array=[];

	ctx.body = {
		status: 200,
		message : "Ok",
		result : "Delete All Success"
	};
})

route.delete('/delete/:id', (ctx) => {
    const paramsId = ctx.params.id;

    array = array.filter(data => data.id !== Number(paramsId));

    ctx.body = {
        status: 200,
        message: 'OK',
        results: 'Delete Success'
    };
})

app.use(route.routes());
app.listen(3000,function(){
	console.log('Server running on http://localhost:3000');
})