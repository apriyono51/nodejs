class Kulkas {
    constructor(id ,name, model) {
        this.id = id;
        this.name = name;
        this.model = model;
        this.type = 'kulkas'
    }
}

module.exports = Kulkas;