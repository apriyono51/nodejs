const tvClass = require('./tv');
const kipasClass = require('./kipas_angin');
const kulkasClass = require('./kulkas');

module.exports = function (request) {

    if (request.type === 'televisi') {
        return new tvClass(request.id, request.name, request.model);
    } else if (request.type === 'kipas') {
        return new kipasClass(request.id, request.name, request.model);
    } else if (request.type === 'kulkas') {
        return new kulkasClass(request.id, request.name, request.model);
    }

};